<!-- The justified navigation menu is meant for single line per list item.
Multiple lines will require custom code not provided by Bootstrap. -->
<div class="masthead">
    <h3 class="text-muted">GSB</h3>
    <nav>
        <ul class="nav nav-justified">
            <ul class="nav nav-justified">
                <li><a href="visiteur.accueil.php">Accueil</a></li>
                <li class="active"><a href="visiteur.saisieFicheFrais.php">Saisir votre note de frais</a></li>
                <li><a href="visiteur.saisieHorsForfait.php">Saisir vos frais hors forfait</a></li>
                <li><a href="visiteur.consulterLesFichesFrais.php">Consulter vos fiche de frais</a></li>
                <li><a href="exit.php">Déconnexion</a></li>
            </ul>
        </ul>
    </nav>
</div>
