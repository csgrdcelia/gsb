<?php

/**
 * Application <Appli-Frais>
 */

/**
 * Représente une ligne de frais d'une fiche de frais
 * 
 * @author pvig <vignard.philippe@laposte.net>
 * @package GSB
 * @version 2.0.0
 * @category Domain class
 */
class LigneFraisHorsForfait {

    /**
     * Obtient la ligne de frais hors forfait
     * 
     * @return string
     */
    public function getIdLigneFraisHorsForfait() {
        return $this->idLigneFraisHorsForfait;
    }

    /**
     * Obtient la date du frais hors forfait
     * 
     * @return DateTime
     */
    public function getDate() {
        return $this->date;
    }

    /**
     * Obtient la fiche de frais concernée par la ligne de frais hors forfait
     * 
     * @return \FicheFrais
     */
    function getFicheFrais() {
        return $this->ficheFrais;
    }

    /**
     * Obtient le montant de la ligne de frais hors forfait
     * 
     * @return string 
     */
    function getMontant() {
        return $this->montant;
    }

    /**
     * Obtient le libellé de la ligne de frais hors forfait
     * 
     * @return string
     */
    function getLibelle() {
        return $this->libelle;
    }

    /**
     * Définit la date du frais hors forfait
     * 
     * @param DateTime $date
     */
    function setDate(DateTime $date) {
        $this->date = $date;
    }

    /**
     * Définit le montant du frais hors forfait
     * 
     * @param string|decimal $montant
     */
    function setMontant($montant) {
        $this->montant = $montant;
    }

    /**
     * Définit le libellé du frais hors forfait
     * 
     * @param string $libelle
     */
    function setLibelle($libelle) {
        $this->libelle = $libelle;
    }

    /**
     * Sauvegarde la ligne de frais hors forfait
     */
    public function save() {
        if ($this->idLigneFraisHorsForfait == null) {
            $this->insert();
        } else {
            $this->update();
        }
    }

    /**
     * Supprime une ligne de frais hors forfait
     */
    public function delete() {
        $pdo = new PDO(DSN, USER, PASSWORD);
        $pdoStatement = $pdo->prepare(LigneFraisHorsForfait::$delete);
        $idLigneFraisHorsForfait = $this->idLigneFraisHorsForfait;
        $pdoStatement->bindParam(":idLigneFraisHorsForfait", $idLigneFraisHorsForfait);
        $pdoStatement->execute();
        unset($pdo);
    }

    /**
     * Retourne une ligne de frais hors forfait à partir d'un identifiant de ligne de frais hors forfait
     * 
     * @param int|string $idLigneFraisHorsForfait Un identifiant de ligne de frais hors forfait
     * @return LigneFraisHorsForfait|null Une ligne de frais hors forfait
     */
    public function fetch($idLigneFraisHorsForfait) {
        $ligneFraisHorsForfait = null;
        $pdo = new PDO(DSN, USER, PASSWORD);
        $pdoStatement = $pdo->prepare(LigneFraisHorsForfait::$selectById);
        $pdoStatement->bindParam(':idLigneFraisHorsForfait', $idLigneFraisHorsForfait);
        $pdoStatement->execute();
        $record = $pdoStatement->fetch(PDO::FETCH_ASSOC);
        if ($record != false) {
            $ligneFraisHorsForfait = LigneFraisHorsForfait::arrayToLigneFraisHorsForfait($record);
        }
        return $ligneFraisHorsForfait;
    }

    /**
     * Retourne les lignes de frais d'une fiche de frais
     * 
     * @param FicheFrais $ficheFrais
     * @return Array les lignes de frais d'une fiche de frais
     */
    public static function fetchAllByFicheFrais(FicheFrais $ficheFrais) {
        $collectionLigneFraisHorsForfait = array();
        $idFicheFrais = $ficheFrais->getIdFicheFrais();
        $pdo = new PDO(DSN, USER, PASSWORD);
        $pdoStatement = $pdo->prepare(LigneFraisHorsForfait::$selectByIdFicheFrais);
        $pdoStatement->bindParam(':idFicheFrais', $idFicheFrais);
        $pdoStatement->execute();
        $recordSet = $pdoStatement->fetchAll();
        foreach ($recordSet as $ligneFraisHorsForfait) {
            $collectionLigneFraisHorsForfait[] = LigneFraisHorsForfait::arrayToLigneFraisHorsForfait($ligneFraisHorsForfait, $ficheFrais);
        }
        unset($pdo);

        return $collectionLigneFraisHorsForfait;
    }

    /**
     * Initialise une ligne de frais hors forfait à partir d'une fiche de frais
     * 
     * @param FicheFrais $ficheFrais
     */
    public function __construct(FicheFrais $ficheFrais) {
        $this->ficheFrais = $ficheFrais;
    }

    private static function arrayToLigneFraisHorsForfait(Array $array, FicheFrais $ficheFrais = null) {
        if ($ficheFrais == null) {
            $ficheFrais = FicheFrais::fetch($array['idFicheFrais']);
        }
        $lfhf = new LigneFraisHorsForfait($ficheFrais);
        $lfhf->date = (new DateTime())->setTimestamp($array['date']);
        $lfhf->idLigneFraisHorsForfait = $array['idLigneFraisHorsForfait'];
        $lfhf->libelle = $array['libelle'];
        $lfhf->montant = $array['montant'];

        return $lfhf;
    }

    private function insert() {
        $pdo = new PDO(DSN, USER, PASSWORD);
        $pdoStatement = $pdo->prepare(LigneFraisHorsForfait::$insert);
        $idFicheFrais = $this->ficheFrais->getIdFicheFrais();
        $timeStamp = $this->date->getTimestamp();
        $montant = $this->montant;
        $libelle = $this->libelle;
        $pdoStatement->bindParam(":idFicheFrais", $idFicheFrais, PDO::PARAM_INT);
        $pdoStatement->bindParam(":date", $timeStamp, PDO::PARAM_INT);
        $pdoStatement->bindParam(":montant", $montant, PDO::PARAM_INT);
        $pdoStatement->bindParam(':libelle', $libelle, PDO::PARAM_STR);
        $pdoStatement->execute();
        $this->idLigneFraisHorsForfait = $pdo->lastInsertId();
        unset($pdo);
    }

    private function update() {
        $pdo = new PDO(DSN, USER, PASSWORD);
        $pdoStatement = $pdo->prepare(LigneFraisHorsForfait::$update);
        $idFicheFrais = $this->ficheFrais->getIdFicheFrais();
        $timeStamp = $this->date->getTimestamp();
        $montant = $this->montant;
        $libelle = $this->libelle;
        $idLigneFraisHorsForfait = $this->idLigneFraisHorsForfait;
        $pdoStatement->bindParam(":idFicheFrais", $idFicheFrais, PDO::PARAM_INT);
        $pdoStatement->bindParam(":date", $timeStamp, PDO::PARAM_INT);
        $pdoStatement->bindParam(":montant", $montant, PDO::PARAM_INT);
        $pdoStatement->bindParam(':libelle', $libelle, PDO::PARAM_STR);
        $pdoStatement->bindParam(':idLigneFraisHorsForfait', $idLigneFraisHorsForfait, PDO::PARAM_INT);
        $pdoStatement->execute();
        unset($pdo);
    }

    private $idLigneFraisHorsForfait;
    private $ficheFrais;
    private $date;
    private $montant;
    private $libelle;
    private static $selectByIdFicheFrais = 'select * from lignefraishorsforfait where idFicheFrais=:idFicheFrais';
    private static $insert = 'insert into lignefraishorsforfait (idFicheFrais,date,montant,libelle) values (:idFicheFrais,:date,:montant,:libelle)';
    private static $update = 'update lignefraishorsforfait set date=:date,montant=:montant,libelle=:libelle where idLigneFraisHorsForfait=:idLigneFraisHorsForfait and idFicheFrais=:idFicheFrais';
    private static $delete = 'delete from lignefraishorsforfait where idLigneFraisHorsForfait=:idLigneFraisHorsForfait';
    private static $selectById = "select * from ligneFraisHorsForfait where idLigneFraisHorsForfait=:idLigneFraisHorsForfait";

}
