-- phpMyAdmin SQL Dump
-- version 4.4.3
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Lun 11 Avril 2016 à 11:08
-- Version du serveur :  5.6.26-log
-- Version de PHP :  5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `gsb`
--
CREATE DATABASE IF NOT EXISTS `gsb` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `gsb`;

-- --------------------------------------------------------

--
-- Structure de la table `etat`
--

CREATE TABLE IF NOT EXISTS `etat` (
  `idEtat` varchar(2) NOT NULL,
  `libelleEtat` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `etat`
--

INSERT INTO `etat` (`idEtat`, `libelleEtat`) VALUES
('EC', 'Fiche en cours de saisie'),
('ET', 'Clôturée. En traitement.'),
('MP', 'En paiement'),
('VA', 'Validé');

-- --------------------------------------------------------

--
-- Structure de la table `fichefrais`
--

CREATE TABLE IF NOT EXISTS `fichefrais` (
  `idFicheFrais` int(11) NOT NULL,
  `moisAnnee` char(6) NOT NULL,
  `nbJustificatifs` int(11) DEFAULT NULL,
  `montantValide` decimal(10,0) DEFAULT NULL,
  `dateModif` int(11) DEFAULT NULL,
  `idVisiteur` varchar(4) NOT NULL,
  `idEtat` varchar(2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Structure de la table `fraisforfait`
--

CREATE TABLE IF NOT EXISTS `fraisforfait` (
  `idFraisForfait` varchar(3) NOT NULL,
  `libelleFraisForfait` varchar(20) NOT NULL,
  `montantFraisForfait` decimal(5,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `fraisforfait`
--

INSERT INTO `fraisforfait` (`idFraisForfait`, `libelleFraisForfait`, `montantFraisForfait`) VALUES
('KM', 'Kilométrage', '5.50'),
('NUI', 'Nuitée', '80.00'),
('RM', 'Repas midi', '29.00');

-- --------------------------------------------------------

--
-- Structure de la table `lignefraisforfait`
--

CREATE TABLE IF NOT EXISTS `lignefraisforfait` (
  `idFraisForfait` varchar(3) NOT NULL,
  `idFicheFrais` int(8) NOT NULL,
  `quantite` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




-- --------------------------------------------------------

--
-- Structure de la table `lignefraishorsforfait`
--

CREATE TABLE IF NOT EXISTS `lignefraishorsforfait` (
  `idFicheFrais` int(11) NOT NULL,
  `idLigneFraisHorsForfait` int(11) NOT NULL,
  `date` int(11) NOT NULL,
  `montant` decimal(10,2) NOT NULL,
  `libelle` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `idUser` varchar(4) NOT NULL,
  `nom` varchar(30) NOT NULL,
  `prenom` varchar(30) NOT NULL,
  `login` varchar(20) NOT NULL,
  `mdp` varchar(20) NOT NULL,
  `adresse` varchar(30) NOT NULL,
  `ville` varchar(30) NOT NULL,
  `cp` varchar(5) NOT NULL,
  `dateEmbauche` date NOT NULL,
  `profil` enum('Visiteur','Comptable') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`idUser`, `nom`, `prenom`, `login`, `mdp`, `adresse`, `ville`, `cp`, `dateEmbauche`, `profil`) VALUES
('a111', 'Gallois', 'Arnaud', 'garnaud', 'Azerty1', '11 rue de la grotte enchantée', 'Paris', '75005', '2010-11-11', 'Comptable'),
('a131', 'Villechalane', 'Louis', 'lvillachane', 'jux7g', '8 rue des Charmes', 'Cahors', '46000', '2005-12-21', 'Visiteur'),
('a17', 'Andre', 'David', 'dandre', 'oppg5', '1 rue Petit', 'Lalbenque', '46200', '1998-11-23', 'Visiteur'),
('a55', 'Bedos', 'Christian', 'cbedos', 'gmhxd', '1 rue Peranud', 'Montcuq', '46250', '1995-01-12', 'Visiteur'),
('a93', 'Tusseau', 'Louis', 'ltusseau', 'ktp3s', '22 rue des Ternes', 'Gramat', '46123', '2000-05-01', 'Visiteur'),
('b13', 'Bentot', 'Pascal', 'pbentot', 'doyw1', '11 allée des Cerises', 'Bessines', '46512', '1992-07-09', 'Visiteur'),
('b16', 'Bioret', 'Luc', 'lbioret', 'hrjfs', '1 Avenue gambetta', 'Cahors', '46000', '1998-05-11', 'Visiteur'),
('b19', 'Bunisset', 'Francis', 'fbunisset', '4vbnd', '10 rue des Perles', 'Montreuil', '93100', '1987-10-21', 'Visiteur'),
('b25', 'Bunisset', 'Denise', 'dbunisset', 's1y1r', '23 rue Manin', 'paris', '75019', '2010-12-05', 'Visiteur'),
('b28', 'Cacheux', 'Bernard', 'bcacheux', 'uf7r3', '114 rue Blanche', 'Paris', '75017', '2009-11-12', 'Visiteur'),
('b34', 'Cadic', 'Eric', 'ecadic', '6u8dc', '123 avenue de la République', 'Paris', '75011', '2008-09-23', 'Visiteur'),
('b4', 'Charoze', 'Catherine', 'ccharoze', 'u817o', '100 rue Petit', 'Paris', '75019', '2005-11-12', 'Visiteur'),
('b50', 'Clepkens', 'Christophe', 'cclepkens', 'bw1us', '12 allée des Anges', 'Romainville', '93230', '2003-08-11', 'Visiteur'),
('b59', 'Cottin', 'Vincenne', 'vcottin', '2hoh9', '36 rue Des Roches', 'Monteuil', '93100', '2001-11-18', 'Visiteur'),
('c14', 'Daburon', 'François', 'fdaburon', '7oqpv', '13 rue de Chanzy', 'Créteil', '94000', '2002-02-11', 'Visiteur'),
('c3', 'De', 'Philippe', 'pde', 'gk9kx', '13 rue Barthes', 'Créteil', '94000', '2010-12-14', 'Visiteur'),
('c54', 'Debelle', 'Michel', 'mdebelle', 'od5rt', '181 avenue Barbusse', 'Rosny', '93210', '2006-11-23', 'Visiteur'),
('d13', 'Debelle', 'Jeanne', 'jdebelle', 'nvwqq', '134 allée des Joncs', 'Nantes', '44000', '2000-05-11', 'Visiteur'),
('d51', 'Debroise', 'Michel', 'mdebroise', 'sghkb', '2 Bld Jourdain', 'Nantes', '44000', '2001-04-17', 'Visiteur'),
('e22', 'Desmarquest', 'Nathalie', 'ndesmarquest', 'f1fob', '14 Place d Arc', 'Orléans', '45000', '2005-11-12', 'Visiteur'),
('e24', 'Desnost', 'Pierre', 'pdesnost', '4k2o5', '16 avenue des Cèdres', 'Guéret', '23200', '2001-02-05', 'Visiteur'),
('e39', 'Dudouit', 'Frédéric', 'fdudouit', '44im8', '18 rue de l église', 'GrandBourg', '23120', '2000-08-01', 'Visiteur'),
('e49', 'Duncombe', 'Claude', 'cduncombe', 'qf77j', '19 rue de la tour', 'La souteraine', '23100', '1987-10-10', 'Visiteur'),
('e5', 'Enault-Pascreau', 'Céline', 'cenault', 'y2qdu', '25 place de la gare', 'Gueret', '23200', '1995-09-01', 'Visiteur'),
('e52', 'Eynde', 'Valérie', 'veynde', 'i7sn3', '3 Grand Place', 'Marseille', '13015', '1999-11-01', 'Visiteur'),
('f21', 'Finck', 'Jacques', 'jfinck', 'mpb3t', '10 avenue du Prado', 'Marseille', '13002', '2001-11-10', 'Visiteur'),
('f39', 'Frémont', 'Fernande', 'ffremont', 'xs5tq', '4 route de la mer', 'Allauh', '13012', '1998-10-01', 'Visiteur'),
('f4', 'Gest', 'Alain', 'agest', 'dywvt', '30 avenue de la mer', 'Berre', '13025', '1985-11-01', 'Visiteur');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `etat`
--
ALTER TABLE `etat`
  ADD PRIMARY KEY (`idEtat`);

--
-- Index pour la table `fichefrais`
--
ALTER TABLE `fichefrais`
  ADD PRIMARY KEY (`idFicheFrais`),
  ADD UNIQUE KEY `u_ficheFraisMois` (`idFicheFrais`,`moisAnnee`),
  ADD KEY `fk_visiteur` (`idVisiteur`),
  ADD KEY `fk_etat` (`idEtat`);

--
-- Index pour la table `fraisforfait`
--
ALTER TABLE `fraisforfait`
  ADD PRIMARY KEY (`idFraisForfait`);

--
-- Index pour la table `lignefraisforfait`
--
ALTER TABLE `lignefraisforfait`
  ADD PRIMARY KEY (`idFraisForfait`,`idFicheFrais`),
  ADD KEY `fk_ficheFrais` (`idFicheFrais`);

--
-- Index pour la table `lignefraishorsforfait`
--
ALTER TABLE `lignefraishorsforfait`
  ADD PRIMARY KEY (`idLigneFraisHorsForfait`),
  ADD KEY `idFicheFrais` (`idFicheFrais`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`idUser`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `fichefrais`
--
ALTER TABLE `fichefrais`
  MODIFY `idFicheFrais` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `lignefraishorsforfait`
--
ALTER TABLE `lignefraishorsforfait`
  MODIFY `idLigneFraisHorsForfait` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `fichefrais`
--
ALTER TABLE `fichefrais`
  ADD CONSTRAINT `fk_etat` FOREIGN KEY (`idEtat`) REFERENCES `etat` (`idEtat`),
  ADD CONSTRAINT `fk_visiteur` FOREIGN KEY (`idVisiteur`) REFERENCES `user` (`idUser`);

--
-- Contraintes pour la table `lignefraisforfait`
--
ALTER TABLE `lignefraisforfait`
  ADD CONSTRAINT `fk_ficheFrais` FOREIGN KEY (`idFicheFrais`) REFERENCES `fichefrais` (`idFicheFrais`),
  ADD CONSTRAINT `fk_fraisForfait` FOREIGN KEY (`idFraisForfait`) REFERENCES `fraisforfait` (`idFraisForfait`);

--
-- Contraintes pour la table `lignefraishorsforfait`
--
ALTER TABLE `lignefraishorsforfait`
  ADD CONSTRAINT `lignefraishorsforfait_ibfk_1` FOREIGN KEY (`idFicheFrais`) REFERENCES `fichefrais` (`idFicheFrais`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
