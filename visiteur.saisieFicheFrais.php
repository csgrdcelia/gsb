<?php require_once 'conf/config.php'; ?>
<!DOCTYPE html>
<html lang="fr">
    <?php include_once 'head.inc.php'; ?>

    <body>

        <div class="container">

<?php include_once 'visiteur.menu.inc.php'; ?>

            <!-- Jumbotron -->
            <div class="jumbotron">
                <h1>Note de frais courante</h1>
                <h3><?php echo date("d-m-Y") ?></h3>
                <p class="lead">
                <form method="post" action="visiteur.traitement.saisieFicheFrais.php">

<?php
$visiteurCourant = $_SESSION["connectedUser"];
$moisAnnee = date('mY');
$ficheFraisCourante = $visiteurCourant->getFicheFrais($moisAnnee);
if($ficheFraisCourante==null)
{
    $ficheFraisCourante = $visiteurCourant->creerFicheFrais($moisAnnee);
}
$_SESSION["ficheFraisCourante"] = $ficheFraisCourante ;


$collectionLigneFraisForfait = $ficheFraisCourante->getCollectionLigneFraisForfait();

foreach ($collectionLigneFraisForfait as $ligneFraisForfait):
    ?>
                        <div class="col-sm-4">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title">
    <?php
    $fraisForfait = $ligneFraisForfait->getFraisForfait();
    echo $fraisForfait->getLibelleFraisForfait();
    ?>
                                    </h3>
                                </div>
                                <div class="panel-body">
                                    <p>
                                        <input type="number" id="<?php echo $ligneFraisForfait->getFraisForfait()->getIdFraisForfait(); ?>" 
                                               name="<?php echo $ligneFraisForfait->getFraisForfait()->getIdFraisForfait(); ?>" 
                                               <?php if ($ligneFraisForfait->getFraisForfait()->getIdFraisForfait() == "NUI"): ?>
                                                   min="0" max="31"
    <?php endif; ?>
                                               pattern="[0-9]+"

                                               value="<?php
    echo $ligneFraisForfait->getQuantite();
    ?>"
                                               />
                                    </p>
                                    <p>
                        <?php //echo $fraisForfaitCourant->montantFicheFrais; ?>
                                    </p>
                                </div>
                            </div>
                        </div>
    <?php
endforeach;
?>

                    <button type="submit" class="btn btn-success">Valider</button>

                </form>
                </p>

            </div>



            <!-- Site footer -->
            <footer class="footer">
                <p>&copy; GSB 2015</p>
            </footer>

        </div> <!-- /container -->


        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
       
    </body>
</html>






