<?php require_once './conf/config.php'; ?>
<!DOCTYPE html>
<html lang="fr">
   <?php include_once 'head.inc.php'; ?>

    <body>

        <div class="container">

            <?php include_once 'visiteur.menu.inc.php'; ?>


            <h1>Consultation des frais hors forfait</h1>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <table class="table table-striped">
                        <?php
                        if (isset($_REQUEST['idFicheFrais'])) {
                            $idFicheFrais = $_REQUEST["idFicheFrais"];
                            $visiteurCourant = $_SESSION["connectedUser"];
                            $ficheFrais = $visiteurCourant->getFicheFraisById($idFicheFrais);
                            $ligneFraisHorsForfait = $ficheFrais->getCollectionLigneFraisHorsForfait();

                            if ($ligneFraisHorsForfait != null):
                                foreach ($ligneFraisHorsForfait as $ligneFraisHorsForfait):
                                    ?>


                                    <tr class="warning">
                                        <td><?php echo $ligneFraisHorsForfait->getLibelle(); ?></td>
                                        <td><?php echo $ligneFraisHorsForfait->getDate()->format('d-m-Y'); ?></td>
                                        <td><?php echo $ligneFraisHorsForfait->getMontant(); ?></td>
                                    </tr>


                                    <?php
                                endforeach;
                            endif;
                        }
                        ?>
                    </table>
                </div>
            </div>
        </div>



        <!-- Site footer -->
        <footer class="footer col-md-offset-1">
            <p>&copy; GSB 2015</p>
        </footer>

    </div> <!-- /container -->


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>



